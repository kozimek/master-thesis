package test;

import static org.junit.Assert.*;

import mt.api.Callable;
import mt.api.Continuation;
import mt.api.Continuations;
import mt.api.Function;

import org.junit.Test;

public class ContinuationsTest {

	@Test
	public void testResetValue() throws Exception {
		int result = Continuations.reset(new Callable<Integer>() {

			@Override
			public
			Integer call() throws Exception {
				return 0;
			}
		});
		assertEquals(0, result);
	}
	
	@Test
	public void testSimpleShift() throws Exception {
		int result = Continuations.reset(new Callable<Void>() {

			@Override
			public
			Void call() throws Exception {
				return Continuations.shift(new Function<Continuation<Void,Void>, Integer>() {

					@Override
					public
					Integer call(Continuation<Void, Void> arg) throws Exception {
						return 0;
					}

					
				});
			}
			
		});
		assertEquals(0, result);
	}
	
	int i = 0;
	int t[] = new int[100];
	void init() {
		i = 0;
		for (int i=0; i<100; i++) {
			t[i] = -1;
		}
	}
	
	@Test
	public void testSimpleCall() throws Exception {
		init();
		
		Continuations.<Integer, Void> reset(new Callable<Integer>() {

			@Override
			public
			Integer call() throws Exception {
				return Continuations.shift(new Function<Continuation<Integer,Integer>, Void>() {

					@Override
					public
					Void call(Continuation<Integer, Integer> arg) throws Exception {
						t[i++] = arg.call(0);
						t[i++] = arg.call(1);
						return null;
					}	
				}) + 1;
				
			}
			
		});
		
		assertEquals(1, t[0]);
		assertEquals(2, t[1]);
	}
	
	Continuation<Integer,Integer> cont;
	
	@Test
	public void testCallOutsideReset() throws Exception {
		Continuations.<Integer, Void> reset(new Callable<Integer>() {

			@Override
			public
			Integer call() throws Exception {
				return Continuations.shift(new Function<Continuation<Integer,Integer>, Void>() {

					@Override
					public
					Void call(Continuation<Integer, Integer> arg) throws Exception {
						cont = arg;
						return null;
					}	
				}) + 1;
				
			}
			
		});
		
		int result0 = cont.call(0);
		assertEquals(1, result0);
		
		int result1 = cont.call(1);
		assertEquals(2, result1);
	}
	
	@Test
	public void shiftInContinuation() throws Exception {
		
		init();
		
		Continuations.reset(new Callable<Integer>() {

			@Override
			public
			Integer call() throws Exception {
				int x = Continuations.shift(new Function<Continuation<Integer,Integer>, Void>() {

					@Override
					public
					Void call(Continuation<Integer,Integer> arg) throws Exception {
						cont = arg;
						return null;
					}
				})

				+ Continuations.shift(new Function<Continuation<Integer,Integer>, Integer>() {

					@Override
					public
					Integer call(Continuation<Integer,Integer> arg) throws Exception {
						t[i++] = arg.call(5);
						t[i++] = arg.call(6);
						return null;
					}
				});
				
				return x;
				
			}
			
		});
		
		cont.call(0);
		cont.call(1);
		
		i = 0;
		assertEquals(5, t[i++]);
		assertEquals(6, t[i++]);
		assertEquals(6, t[i++]);
		assertEquals(7, t[i++]);
	}
	
	private int f(int x) {
		Continuations.shift(new Function<Continuation<Integer,Integer>, Integer>() {
			@Override
			public
			Integer call(Continuation<Integer,Integer> arg) throws Exception {
				return 0;
			}
		});
		return 0;
	}
	
	@Test
	public void testPrivateMethod() throws Exception {
		
		init();
		
		int result = Continuations.reset(new Callable<Void>() {

			@Override
			public
			Void call() throws Exception {
				f(0);
				return null;
			}
			
		});
		
		assertEquals(0, result);
	}
	
	@Test
	public void testShiftIsNotControl() throws Exception {
		
		init();
		
		int result = Continuations.reset(new Callable<Integer>() {

			@Override
			public
			Integer call() throws Exception {
				
				final int x = 3 * Continuations.shift(new Function<Continuation<Integer, Integer>, Integer>() {

					@Override
					public
					Integer call(Continuation<Integer, Integer> arg) throws Exception {
						return 2 * arg.call(7);
					}
				});
				
				return 5 * Continuations.shift(new Function<Continuation<Integer,Integer>, Integer>() {

					@Override
					public
					Integer call(Continuation<Integer, Integer> arg)
							throws Exception {
						return x;
					}
				});
			}

			
			
		});
		
		assertEquals(42, result);
	}
	
	@Test
	public void testShiftIsNotShift0() throws Exception {
		
		init();
		
		int result = Continuations.reset(new Callable<Integer>() {

			@Override
			public
			Integer call() throws Exception {
				
				return 2 * Continuations.shift(new Function<Continuation<Integer, Integer>, Integer>() {

					@Override
					public
					Integer call(Continuation<Integer, Integer> arg) throws Exception {
						
						return 3 * Continuations.shift(new Function<Continuation<Integer,Integer>, Integer>() {

							@Override
							public
							Integer call(Continuation<Integer, Integer> arg)
									throws Exception {
								return arg.call(1);
							}
						});

						
					}
				});
			}

			
			
		});
		
		assertEquals(3, result);
	}
	
	@Test
	public void testResetInsideShift() throws Exception {
		
		init();
		
		int result = Continuations.reset(new Callable<Integer>() {

			@Override
			public
			Integer call() throws Exception {
				
				return Continuations.shift(new Function<Continuation<Integer,Integer>, Integer>() {

					@Override
					public
					Integer call(Continuation<Integer, Integer> arg) throws Exception {

						return 1 + Continuations.<Integer,Integer>reset(new Callable<Integer>() {

							@Override
							public
							Integer call() throws Exception {
								return Continuations.shift(new Function<Continuation<Integer,Integer>, Integer>() {

									@Override
									public
									Integer call(Continuation<Integer, Integer> arg) throws Exception {
										return 1;
									}
								});
							}
						});
						
					}
				});
				
			}
			
		});
		
		assertEquals(2, result);
	}
	
	@SuppressWarnings("serial")
	class TestException extends Exception {}
	
	@Test(expected=TestException.class)
	public void testExceptionInShift() throws Exception {
		
		Continuations.reset(new Callable<Void>() {
			@Override
			public
			Void call() throws Exception {
				return Continuations.shift(new Function<Continuation<Void,Void>, Integer>() {

					@Override
					public
					Integer call(Continuation<Void, Void> arg) throws Exception {
						throw new TestException();
					}
				});
			}
			
		});
	}
	
	@Test(expected=TestException.class)
	public void testExceptionInReset() throws Exception {
		
		Continuations.reset(new Callable<Void>() {
			@Override
			public
			Void call() throws Exception {
				throw new TestException();
			}
			
		});
	}
	
	boolean caught = false;
	
	@Test
	public void testExceptionInContinuation() throws Exception {
		
		caught = false;
		
		Continuations.reset(new Callable<Void>() {
			@Override
			public
			Void call() throws Exception {
				Continuations.shift(new Function<Continuation<Void,Void>, Void>() {

					@Override
					public
					Void call(Continuation<Void, Void> arg) throws Exception {
						try {
							arg.call(null);
						} catch (TestException e) {
							caught = true;
						}
						return null;
					}
				});
				
				Continuations.shift(new Function<Continuation<Void,Void>, Integer>() {

					@Override
					public
					Integer call(Continuation<Void, Void> arg) throws Exception {
						throw new TestException();
					}
				});
				
				return null;
			}
			
		});
		
		assertEquals(true, caught);
	}

}
