package mt.api;

import mt.internal.Transform;

public interface Callable<T> {
	@Transform
	T call() throws Exception;
}
