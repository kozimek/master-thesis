package mt.api;

import mt.internal.Trampoline;
import mt.internal.Transform;

import org.objectweb.asm.Opcodes;

public class Continuations {

	public static <B, C> C reset(final Callable<B> thunk) throws Exception {
		try {
			Class<?> contClass = Class
					.forName("mt.internal.mt.api.Continuations._reset(Lmt.api.Callable))V_Continuation");
									
			mt.internal.Continuation<?,?> cont = (mt.internal.Continuation<?,?>) contClass
				.newInstance();
			cont._this = thunk;
			cont.reset = true;
			cont.locals = new Object[] { thunk };
			cont.opcode = Opcodes.INVOKESTATIC;
			return Trampoline.runToReset(cont.Continue());

		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static <A, B, C> A shift(final Function<Continuation<A, B>, C> thunk) {
		return null;
	}

	
	@Transform
	private static void _reset(final Callable<?> thunk) throws Exception {
		thunk.call();
	}
	
	
	public static <A> A callCC(final Function<Function<A, Void>, A> thunk) {
		return Continuations.shift(new Function<Continuation<A,Void>, Void>() {

			@Override
			public
			Void call(final Continuation<A, Void> pc) throws Exception {
				return pc.call(thunk.call(new Function<A,Void>() {

					@Override
					public
					Void call(final A v) throws Exception {
						return Continuations.shift(new Function<Continuation<Void,Void>, Void>() {

							@Override
							public
							Void call(Continuation<Void, Void> arg)
									throws Exception {
								return pc.call(v);
							}
						});
					}
					
				}));
			}
			
		});
	}
}
