package mt.api;

public interface Continuation<A,B> {
	B call(A arg) throws Exception;
}
