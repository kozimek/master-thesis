package mt.api;

import mt.internal.Transform;

public interface Function<A,B> {
	@Transform
	B call(A arg) throws Exception;

	
}
