package mt.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class TransformAllMethods implements MethodSelector {

	private String[] _noTransform = new String[] { "mt/internal/",
			Utils.apiPackageName + "/Continuation/call",
			Utils.apiPackageName + "/Continuations", "sun/launcher/" };

	private ArrayList<String> noTransform = new ArrayList<>(
			Arrays.asList(_noTransform));

	String main;

	public TransformAllMethods(Collection<String> nos, String main) {
		noTransform.addAll(nos);
		this.main = main;
	}

	public Set<String> methodsToTransform = new HashSet<>();

	@Override
	public boolean transform(String owner, String name, String desc) {
		String methodName = owner + "/" + name + desc;
		for (String s : noTransform) {
			if (main != null) {
				if (methodName.startsWith(main + "/")) {
					return false;
				}
			}
			if (methodName.startsWith(s)) {
				return false;
			}
			if (methodName.contains("<")) {
				return false;
			}
		}

		return true;
	}

	@Override
	public void init() {
	}

}
