package mt.internal;

public class Trampoline {
	
	public static <T> T runToReset(Continuation<?, ?> c) throws Exception {
		
		//boolean reset = false;
		Continuation<?, ?> prev = null;
		//System.out.println(c);
		while ((c != null)) {
			if (c.reset && c.end) {
				break;
			}
			prev = c;
			c = c.Continue();
			//System.out.println(c);
		}
		
		
		exception(prev.resultException);
		
		
		
		@SuppressWarnings("unchecked")
		T result = (T) prev.result;
		return result;

		
	}
	
	private static void exception(Throwable throwable) throws Exception {
		if (throwable != null) {
			if (throwable instanceof Error) {
				throw (Error)throwable;
			}
			if (throwable instanceof RuntimeException) {
				throw (RuntimeException)throwable;
			}
			throw (Exception)throwable;
		}
	}
	
	
}
