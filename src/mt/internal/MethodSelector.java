package mt.internal;

public interface MethodSelector {
	boolean transform(String owner, String name, String desc);
	void init();
}
