package mt.internal;

import org.objectweb.asm.ClassReader;


public class ClassWriter extends org.objectweb.asm.ClassWriter {

	public ClassWriter(ClassReader arg0, int arg1) {
		super(arg0, arg1);
	}
	
	public ClassWriter(int arg0) {
		super(arg0);
	}
	
	@Override
	protected String getCommonSuperClass(String arg0, String arg1) {
		
		
		if (arg0.equals("java/lang/Object") || arg1.equals("java/lang/Object")) {
			return "java/lang/Object";
		}
		
		return super.getCommonSuperClass(arg0, arg1);
		
	}

}
