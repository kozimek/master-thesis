package mt.internal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.AnalyzerException;

class StateMachineTransformer implements ClassFileTransformer {

	public static class Config {
		public MethodSelector methodSelector;
		public boolean saveTransformedClassesToFile = false;
		public String pathToSaveTransformedClasses = ".";
	}
	private Config config;

	public StateMachineTransformer(Config config) {
		this.config = config;
	}

	@Override
	public byte[] transform(ClassLoader loader, final String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {

		ClassReader cr = new ClassReader(classfileBuffer);

		ClassNode cn = new ClassNode(Opcodes.ASM5);
		cr.accept(cn, 0);

		cn.access &= (~Opcodes.ACC_PRIVATE);
		cn.access &= (~Opcodes.ACC_PROTECTED);
		cn.access |= Opcodes.ACC_PUBLIC;

		boolean transformed = false;

		ArrayList<MethodNode> methodsToAdd = new ArrayList<>();
		ArrayList<ClassNode> classesToLoad = new ArrayList<>();

		@SuppressWarnings("unchecked")
		List<MethodNode> methods = (List<MethodNode>) cn.methods;
		for (MethodNode mn : methods) {

			if (Utils.isAnnotated(mn, Transform.class)
					|| config.methodSelector.transform(className, mn.name, mn.desc)) {

				try {

					MethodNode cont = new StateMachineBuilder(className, mn, config.methodSelector)
							.generate();
					methodsToAdd.add(cont);
					transformed = true;
				} catch (AnalyzerException e) {
					throw new RuntimeException(e);
				}

				ClassNode cont = new ContinuationBuilder(className, mn).build();
				classesToLoad.add(cont);

			}
		}
		if (!transformed) {
			return null;
		}
		for (MethodNode mn : methodsToAdd) {
			@SuppressWarnings({ "unused", "unchecked" })
			boolean add = cn.methods.add(mn);
		}
		ClassWriter cw = new mt.internal.ClassWriter(cr,
				ClassWriter.COMPUTE_FRAMES + ClassWriter.COMPUTE_MAXS);
		try {
			cn.accept(cw);
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (ClassNode ctl : classesToLoad) {
			ClassWriter cw2 = new mt.internal.ClassWriter(
					ClassWriter.COMPUTE_FRAMES + ClassWriter.COMPUTE_MAXS);
			try {
				ctl.accept(cw2);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			if (config.saveTransformedClassesToFile) {
				File file = new File(config.pathToSaveTransformedClasses + "/"
						+ ctl.name.replace('/', '.') + ".class");
				FileOutputStream fos;
				try {
					fos = new FileOutputStream(file);
					fos.write(cw2.toByteArray());
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			Utils.loadClass(ctl.name.replace('/', '.'), cw2.toByteArray());

			try {
				Class.forName(ctl.name.replace('/', '.'));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		}

		if (transformed) {
			if (config.saveTransformedClassesToFile) {
				File file = new File(config.pathToSaveTransformedClasses + "/"
						+ className.replace('/', '.') + ".class");
				FileOutputStream fos;
				try {
					fos = new FileOutputStream(file);
					fos.write(cw.toByteArray());
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return cw.toByteArray();
		}

		return null;

	}

}
