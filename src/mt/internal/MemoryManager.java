package mt.internal;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.IincInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.tree.analysis.Frame;

public class MemoryManager {
	int maxLocals;
	int maxStack;
	
	public MemoryManager(int maxLocals, int maxStack) {
		this.maxLocals = maxLocals;
		this.maxStack = maxStack;
	}

	public void loadLocal(InsnList code, String desc, int index) {
		code.add(new VarInsnNode(loadOpcode(desc), index));
	}

	public void loadTemp(InsnList code, String desc, int index) {
		loadLocal(code, desc, maxLocals + index + 2);
	}

	public void storeLocal(InsnList code, String desc, int index) {
		code.add(new VarInsnNode(storeOpcode(desc), index));
	}

	public void storeTemp(InsnList code, String desc, int index) {
		storeLocal(code, desc, maxLocals + index + 2);
	}
	
	public void storeTop(InsnList code, Frame frame, int index) {
	
		BasicValue value = ((BasicValue) frame.getStack(frame.getStackSize() - 1));
		String desc = value.getType().getDescriptor();
		
		storeTemp(code, desc, index);
	}
	
	public void loadTop(InsnList code, Frame frame, int index) {
		
		BasicValue value = ((BasicValue) frame.getStack(frame.getStackSize() - 1));
		String desc = value.getType().getDescriptor();
		
		loadTemp(code, desc, index);
	}

	void box(InsnList code, String desc) {
		String boxClass = boxClass(desc);
		storeTemp(code, desc, -2);
		code.add(new TypeInsnNode(Opcodes.NEW, boxClass));
		code.add(new InsnNode(Opcodes.DUP));
		loadTemp(code, desc, -2);
		code.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, boxClass, "<init>",
				"(" + desc + ")V", false));
	}

	void unbox(InsnList code, String desc) {
		String boxClass = boxClass(desc);
		String unboxMethod = unboxMethod(desc);

		code.add(new TypeInsnNode(Opcodes.CHECKCAST, boxClass));
		code.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, boxClass,
				unboxMethod, "()" + desc, false));
	}
	
	void iincTemp(InsnList code, int index) {
		code.add(new IincInsnNode(maxLocals+index+2, 1));
	}

	private int loadOpcode(String desc) {
		switch (desc) {
		case "I":
		case "Z":
			return Opcodes.ILOAD;
		case "J":
			return Opcodes.LLOAD;
		case "F":
			return Opcodes.FLOAD;
		case "D":
			return Opcodes.DLOAD;
		default:
			return Opcodes.ALOAD;
		}
	}

	private int storeOpcode(String desc) {
		switch (desc) {
		case "I":
		case "Z":
			return Opcodes.ISTORE;
		case "J":
			return Opcodes.LSTORE;
		case "F":
			return Opcodes.FSTORE;
		case "D":
			return Opcodes.DSTORE;
		default:
			return Opcodes.ASTORE;
		}
	}

	private String boxClass(String desc) {
		switch (desc) {
		case "I":
			return "java/lang/Integer";
		case "Z":
			return "java/lang/Boolean";
		case "J":
			return "java/lang/Long";
		case "F":
			return "java/lang/Float";
		case "D":
			return "java/lang/Double";
		}
		return null;
	}

	private String unboxMethod(String desc) {
		switch (desc) {
		case "I":
			return "intValue";
		case "Z":
			return "booleanValue";
		case "J":
			return "longValue";
		case "F":
			return "floatValue";
		case "D":
			return "doubleValue";
		}
		return null;
	}

}
