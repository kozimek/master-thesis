package mt.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.AnnotationNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

public class SimpleClasspathScanner implements MethodSelector, Opcodes {

	class Class {
		Class(String name) {
			this.name = name;
		}

		String name;
		Class base;

		boolean _interface = false;
		boolean inClasspath = false;

		ArrayList<Class> interfaces = new ArrayList<>();
		Map<String, Method> methods = new HashMap<>();
		ArrayList<Class> derivedClasses = new ArrayList<>();
		ArrayList<Class> implementingClasses = new ArrayList<>();

		Method getMethod(String name, String desc) {
			String key = name + desc;
			if (!methods.containsKey(key)) {
				Method method = new Method(this, name, desc);
				methods.put(key, method);
				return method;
			} else {
				return methods.get(key);
			}
		}

		boolean alsoHasMehtod(Method method) {
			return methods.containsKey(method.name + method.desc);
		}
	}

	class Method {
		Method(Class owner, String name, String desc) {
			this.owner = owner;
			this.name = name;
			this.desc = desc;
		}

		boolean transform = false;
		boolean _private = false;
		String name;
		Class owner;
		String desc;
		ArrayList<Method> callers = new ArrayList<>();
		ArrayList<Method> callees = new ArrayList<>();

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();

			sb.append(owner.name.replace('/', '.') + "." + name + desc + ": "
					+ transform);
			return sb.toString();
		}
	}

	private Map<String, Class> classes = new HashMap<>();

	private Class getClass(String name) {
		if (!classes.containsKey(name)) {
			Class clazz = new Class(name);
			classes.put(name, clazz);
			return clazz;
		} else {
			return classes.get(name);
		}
	}

	@Override
	public boolean transform(String owner, String name, String desc) {
		return getClass(owner).getMethod(name, desc).transform;
	}

	@Override
	public void init() {
		ClassLoader cl = ClassLoader.getSystemClassLoader();
		if (!(cl instanceof URLClassLoader)) {
			throw new RuntimeException("Can only run with URLClassLoader.");
		}

		URL[] urls = ((URLClassLoader) cl).getURLs();

		for (URL url : urls) {
			if (url.getFile().endsWith(".jar")) {
				handleJar(url);
			} else {
				handleDir(url);
			}
		}

		Queue<Method> queue = new LinkedList<>();

		for (Class c : classes.values()) {
			for (Method m : c.methods.values()) {
				if (m.transform) {
					queue.add(m);
				}
			}
		}

		while (!queue.isEmpty()) {
			Method method = queue.poll();		
			handleMethod(method, queue);
		}

	}
	

	private interface ClassList {
		ArrayList<Class> get(Class clazz);
	}

	private void handleMethod(Method method, Queue<Method> queue) {

		for (Method caller : method.callers) {
			if (!caller.transform) {
				caller.transform = true;
				queue.add(caller);
			}
		}
		
		handleClassList(method, new ClassList() {
			@Override
			public ArrayList<Class> get(Class clazz) {
				return clazz.derivedClasses;
			}
		}, queue);
		handleClassList(method, new ClassList() {
			@Override
			public ArrayList<Class> get(Class clazz) {
				return clazz.implementingClasses;
			}
		}, queue);
		handleClassList(method, new ClassList() {
			@Override
			public ArrayList<Class> get(Class clazz) {
				return clazz.interfaces;
			}
		}, queue);
		handleClassList(method, new ClassList() {
			@Override
			public ArrayList<Class> get(Class clazz) {
				ArrayList<Class> l = new ArrayList<>();
				l.add(clazz.base);
				return l;
			}
		}, queue);
	}
	
	private void handleClassList(Method method, ClassList list, Queue<Method> queue) {
		
		Queue<Class> classQueue = new LinkedList<>();
		if (method.owner != null) {
			classQueue.add(method.owner);
		}
		while (!classQueue.isEmpty()) {
			Class clazz = classQueue.poll();
			
			if (clazz == method.owner 
					|| !clazz.alsoHasMehtod(method)) {
				for (Class i : list.get(clazz)) {
					if (i != null) {
						classQueue.add(i);
					}
				}
			} else {
				Method m = clazz.getMethod(method.name, method.desc);
				if (!m.transform) {
					m.transform = true;
					queue.add(m);
				}
			}
		}
	}

	private void handleDir(URL url) {
		File file = new File(url.getFile());
		Stack<File> stack = new Stack<>();
		stack.add(file);

		while (!stack.empty()) {
			File f = stack.pop();
			if (f.isFile()) {
				if (f.getName().endsWith(".class")) {
					try (InputStream stream = new FileInputStream(f)) {
						handleStream(stream);
					} catch (IOException e) {
						throw new RuntimeException("Cannot read class file "
								+ f.getAbsoluteFile());

					}
				}
			} else {
				for (File child : f.listFiles()) {
					stack.add(child);
				}
			}
		}
	}

	private void handleJar(URL url) {

		try (JarFile file = new JarFile(url.getFile())) {
			Enumeration<JarEntry> entries = file.entries();

			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				if (entry.getName().endsWith(".class")) {
					try (InputStream stream = file.getInputStream(entry)) {
						handleStream(stream);
					}
				}
			}
		} catch (IOException e1) {
			throw new RuntimeException("Cannot read jar file " + url.getFile());
		}
	}

	private void handleStream(InputStream inputStream) {

		ClassReader reader;
		try {
			reader = new ClassReader(inputStream);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		ClassNode classNode = new ClassNode(ASM5);
		reader.accept(classNode, 0);

		Class clazz = getClass(classNode.name);
		clazz.inClasspath = true;

		Class base = getClass(classNode.superName);
		clazz.base = base;

		if ((classNode.access & ACC_INTERFACE) != 0) {
			clazz._interface = true;
		}

		@SuppressWarnings("unchecked")
		List<String> interfaces = classNode.interfaces;
		for (String interfaceName : interfaces) {
			Class _interface = getClass(interfaceName);
			clazz.interfaces.add(_interface);
			_interface.implementingClasses.add(clazz);
		}

		@SuppressWarnings("unchecked")
		List<MethodNode> methods = classNode.methods;
		for (MethodNode methodNode : methods) {
			Method method = clazz.getMethod(methodNode.name, methodNode.desc);

			if (methodNode.invisibleAnnotations != null) {
				for (Object _annotation : methodNode.invisibleAnnotations) {
					AnnotationNode annotation = (AnnotationNode) _annotation;
					if (annotation.desc.equals("Lmt/internal/Transform;")) {
						method.transform = true;
					}
				}
			}

			if ((methodNode.access & ACC_PRIVATE) != 0) {
				method._private = true;
			}

			if ((methodNode.access & ACC_ABSTRACT) != 0) {
				continue;
			}

			InsnList insns = methodNode.instructions;
			@SuppressWarnings("unchecked")
			Iterator<AbstractInsnNode> i = insns.iterator();
			while (i.hasNext()) {
				AbstractInsnNode insn = i.next();
				if (insn.getType() == InsnNode.METHOD_INSN) {
					MethodInsnNode minsn = (MethodInsnNode) insn;
					if (minsn.owner.equals(Utils.apiPackageName + "/Continuations")
							&& minsn.name.equals("shift")) {
						method.transform = true;
					} else {
						Class owner = getClass(minsn.owner);
						Method callee = owner.getMethod(minsn.name, minsn.desc);

						callee.callers.add(method);
						method.callees.add(callee);
					}
				}
			}

		}
	}
}
