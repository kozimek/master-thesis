package mt.internal;

import java.lang.instrument.Instrumentation;
import java.util.ArrayList;

import mt.internal.StateMachineTransformer.Config;

public class Agent {

	public static void premain(String args, Instrumentation instrumentation) {
		
		/*ArrayList<String> classes = new ArrayList<String>();
		for (Class<?> clazz : instrumentation.getAllLoadedClasses())
			if (instrumentation.isModifiableClass(clazz)) {
				classes.add(clazz.getName().replace('.', '/'));
				System.out.println(clazz.getName());
			}*/
		
		
		
		MethodSelector ms = new SimpleClasspathScanner();
		ms.init();
		
		Config config = new Config();
		config.methodSelector = ms;
		
		instrumentation.addTransformer(new StateMachineTransformer(config), true);
		try {
			Class.forName("mt.api.Callable");
			Class.forName("mt.api.Function");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}


}
