package mt.internal;

import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.tree.analysis.Frame;

public class StateCodeGenerator implements Opcodes {

	private InsnList code;
	private MemoryManager memory;

	public StateCodeGenerator(InsnList code, MemoryManager memory) {
		this.code = code;
		this.memory = memory;
	}

	public static void getField(InsnList code, String name, String desc) {
		code.add(new FieldInsnNode(GETFIELD,
				Utils.continuationClassName, name, desc));
	}

	public static void putField(InsnList code, String name, String desc) {
		code.add(new FieldInsnNode(PUTFIELD,
				Utils.continuationClassName, name, desc));
	}

	public static void getStack(InsnList code) {
		getField(code, "stack", "[Ljava/lang/Object;");
	}

	public static void getLocals(InsnList code) {
		getField(code, "locals", "[Ljava/lang/Object;");
	}

	public static void putStack(InsnList code) {
		putField(code, "stack", "[Ljava/lang/Object;");
	}

	public static void putLocals(InsnList code) {
		putField(code, "locals", "[Ljava/lang/Object;");
	}
	
	public void cloneParent(InsnList code) {
		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		StateCodeGenerator.getField(code, "parent", Utils.continuationClassDesc);
		code.add(new InsnNode(DUP));
		
		LabelNode parentNull = new LabelNode(new Label());
		code.add(new JumpInsnNode(IFNULL, parentNull));
		
		code.add(new MethodInsnNode(INVOKEVIRTUAL,
				Utils.continuationClassName, "clone", "()" + Utils.continuationClassDesc,
				false));
		
		code.add(parentNull);
	}

	public void restoreLocals(Frame frame) {
		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		// stack: ... state

		getLocals(code);
		// stack: ... state.locals

		memory.storeTemp(code, "[Ljava/lang/Object;", 1);
		// stack: ...
		// temp: state state.locals
		int j = 0;
		for (int i = 0; i < frame.getLocals(); i++) {
		
			BasicValue v = (BasicValue) frame.getLocal(i);

			if (v.getType() == null) {
				// local is not used or part of two-byte value, continue
				continue;
			}
			
			memory.loadTemp(code, "[Ljava/lang/Object;", 1);
			// stack: ... state.locals

			code.add(new LdcInsnNode(j));
			// stack: ... state.locals j

			code.add(new InsnNode(Opcodes.AALOAD));
			// stack: ... state.locals[j]
			
			String desc = v.getType().getDescriptor();
			if (v.getType().getSort() != Type.OBJECT) {
				memory.unbox(code, desc);
			}
			memory.storeLocal(code, desc, i);

			j++;

		}
	}

	public void restoreStack(Frame frame, MethodInsnNode method) {

		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		// stack: ... state

		getStack(code);
		// stack: ... state.stack

		memory.storeTemp(code, "[Ljava/lang/Object;", 1);
		// stack: ...
		// temp: state state.stack

		
		int n = 0;
		for (int i = 0; i < frame.getStackSize(); i++) {
			BasicValue v = (BasicValue) frame.getStack(i);

			if (v.getType() != null) {
				n++;
			}
		}
		
		int j = 0;
		for (int i = 0; i < frame.getStackSize(); i++) {
			
			BasicValue v = (BasicValue) frame.getStack(i);
			
			if (v.getType() == null) {
				continue;
			}
			
			memory.loadTemp(code, "[Ljava/lang/Object;", 1);
			// stack: ... state.stack

			code.add(new LdcInsnNode(j));
			// stack: ... state.stack j

			code.add(new InsnNode(Opcodes.AALOAD));
			// stack: ... state.stack[j]

			String desc = v.getType().getDescriptor();
			if (v.getType().getSort() != Type.OBJECT) {
				memory.unbox(code, desc);
			} else {
				if (method != null) {
					Type t = Type.getMethodType(method.desc);
					Type[] args = t.getArgumentTypes();
					int nArgs = args.length;
					
					if (i == n - nArgs - 1) {
						// cast this
						if (method.getOpcode() != Opcodes.INVOKESTATIC) {
							code.add(new TypeInsnNode(Opcodes.CHECKCAST,
									method.owner));
						}
					}
					
					if (i >= n - nArgs) {
						// cast argument
						code.add(new TypeInsnNode(Opcodes.CHECKCAST, args[i
								- (n - nArgs)].getInternalName()));
						
					}
				}
			}
			j++;
		}
	}

	public void saveLocals(Frame frame) {

		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		// stack: ... state

		getLocals(code);
		// stack: ... state.locals

		memory.storeTemp(code, "[Ljava/lang/Object;", 1);
		// stack: ...
		// temp: state state.locals

		int j = 0;
		for (int i = 0; i < frame.getLocals(); i++) {

			BasicValue v = (BasicValue) frame.getLocal(i);

			if (v.getType() == null) {
				continue;
			}
			String desc = v.getType().getDescriptor();

			memory.loadTemp(code, "[Ljava/lang/Object;", 1);
			code.add(new LdcInsnNode(j));

			memory.loadLocal(code, desc, i);
			if (v.getType().getSort() != Type.OBJECT) {
				memory.box(code, desc);
			}
			code.add(new TypeInsnNode(Opcodes.CHECKCAST, "java/lang/Object"));
			code.add(new InsnNode(Opcodes.AASTORE));
			j++;
		}
	}

	public void prepareCall(Frame frame, MethodInsnNode method) {

		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		// stack: ... state

		getStack(code);
		// stack: ... state.stack

		memory.storeTemp(code, "[Ljava/lang/Object;", 1);
		// stack: ...
		// temp: state state.stack

		Type t = Type.getMethodType(method.desc);
		Type[] args = t.getArgumentTypes();
		int nArgs = args.length;

		int j = 0;
		int i;
		for (i = frame.getStackSize() - 1;; i--) {

			if (j >= nArgs)
				break;

			BasicValue v = (BasicValue) frame.getStack(i);

			if (v.getType() == null) {
				continue;
			}

			String desc = v.getType().getDescriptor();
			if (v.getType().getSort() != Type.OBJECT) {
				memory.box(code, desc);
			}
			memory.storeTemp(code, "Ljava/lang/Object;", 2);

			memory.loadTemp(code, "[Ljava/lang/Object;", 1);
			code.add(new LdcInsnNode(j));

			memory.loadTemp(code, "Ljava/lang/Object;", 2);
			code.add(new InsnNode(Opcodes.AASTORE));

			j++;
		}

		boolean castThis = !(method.getOpcode() == Opcodes.INVOKESTATIC
				|| method.name.contains("<init>"));
		
		
		if (castThis) {
			BasicValue v = (BasicValue) frame.getStack(i);
			if (v.getType().getSort() == Type.OBJECT) {
				code.add(new TypeInsnNode(Opcodes.CHECKCAST, method.owner));
			}
		}
		
		int k = 0;
		for (int l = i + 1; l < frame.getStackSize(); l++) {

			BasicValue v = (BasicValue) frame.getStack(l);

			if (v.getType() == null) {
				continue;
			}
			
			memory.loadTemp(code, "[Ljava/lang/Object;", 1);
			// stack: ... state.stack
			code.add(new LdcInsnNode(nArgs-1-k));
			// stack: ... state.stack j
			
			code.add(new InsnNode(Opcodes.AALOAD));
			// stack: ... state.stack[j]

			String desc = v.getType().getDescriptor();

			if (v.getType().getSort() != Type.OBJECT) {

				memory.unbox(code, desc);
				
			} else {
				code.add(new TypeInsnNode(Opcodes.CHECKCAST, args[k]
						.getInternalName()));
			}

			k++;

		}
	}
	
	public void saveStack(Frame frame, MethodInsnNode method) {
		
		int n = 0;
		for (int i=0; i<frame.getStackSize(); i++) {
			if (((BasicValue)frame.getStack(i)).getType() != null) {
				n++;
			}
		}
		
		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		// stack: ... state

		getStack(code);
		// stack: ... state.stack

		memory.storeTemp(code, "[Ljava/lang/Object;", 1);
		// stack: ...
		// temp: state state.stack
		
		int j = n-1;
		for (int i = frame.getStackSize() - 1; i >= 0; i--) {
			BasicValue v = (BasicValue) frame.getStack(i);

			if (v.getType() == null) {
				continue;
			}

			String desc = v.getType().getDescriptor();
			if (v.getType().getSort() != Type.OBJECT) {
				memory.box(code, desc);
			}
			memory.storeTemp(code, "Ljava/lang/Object;", 2);

			memory.loadTemp(code, "[Ljava/lang/Object;", 1);
			code.add(new LdcInsnNode(j));

			memory.loadTemp(code, "Ljava/lang/Object;", 2);
			code.add(new InsnNode(Opcodes.AASTORE));

			j--;
		}
		
		restoreStack(frame, method);
		
	}
	
}
