package mt.internal;

import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AnnotationNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

public class Utils {

	static final String packageName = "mt/internal";
	static final String apiPackageName = "mt/api";
	static final String continuationClassName = packageName + "/Continuation";
	static final String continuationClassDesc = "L" + continuationClassName
			+ ";";

	static void debug(InsnList code, String string) {
		code.add(new FieldInsnNode(Opcodes.GETSTATIC, "java/lang/System",
				"out", "Ljava/io/PrintStream;"));
		code.add(new LdcInsnNode(string));
		code.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL,
				"java/io/PrintStream", "println", "(Ljava/lang/String;)V",
				false));

	}

	static void getStream(InsnList code) {
		code.add(new FieldInsnNode(Opcodes.GETSTATIC, "java/lang/System",
				"out", "Ljava/io/PrintStream;"));
	}

	static void println(InsnList code, String desc) {
		code.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL,
				"java/io/PrintStream", "println", "(" + desc + ")V", false));

	}

	static boolean isAnnotated(MethodNode mn, Class<?> annotation) {

		boolean annotated = false;

		if (mn.invisibleAnnotations != null) {
			@SuppressWarnings("unchecked")
			List<AnnotationNode> invisibleAnnotations = (List<AnnotationNode>) mn.invisibleAnnotations;
			for (AnnotationNode an : invisibleAnnotations) {
				if (an.desc.equals(Type.getDescriptor(annotation))) {
					annotated = true;
				}
			}
		}
		return annotated;
	}

	/*
	 * taken from ASM FAQ
	 */
	static Class<?> loadClass(String className, byte[] b) {
		// override classDefine (as it is protected) and define the class.
		Class<?> clazz = null;
		try {
			ClassLoader loader = ClassLoader.getSystemClassLoader();
			Class<?> cls = Class.forName("java.lang.ClassLoader");
			java.lang.reflect.Method method = cls.getDeclaredMethod(
					"defineClass", new Class[] { String.class, byte[].class,
							int.class, int.class });

			// protected method invocaton
			method.setAccessible(true);
			try {
				Object[] args = new Object[] { className, b, new Integer(0),
						new Integer(b.length) };
				clazz = (Class<?>) method.invoke(loader, args);
			} finally {
				method.setAccessible(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return clazz;
	}
}
