package mt.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TableSwitchInsnNode;
import org.objectweb.asm.tree.TryCatchBlockNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.BasicInterpreter;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.tree.analysis.Frame;

public class StateMachineBuilder implements Opcodes {

	private String className;
	private MethodNode method;
	private MemoryManager memory;
	private Frame[] frames;
	boolean _static = false;

	boolean _interface = false;

	LabelNode start = new LabelNode(new Label());
	LabelNode end = new LabelNode(new Label());
	LabelNode handler = new LabelNode(new Label());
	LabelNode resetNotFound = new LabelNode(new Label());

	InsnList code = new InsnList();
	StateCodeGenerator state;
	ArrayList<LabelNode> labels = new ArrayList<>();
	Map<LabelNode, LabelNode> labelMap = new HashMap<>();
	private MethodSelector ms;

	public StateMachineBuilder(String className, MethodNode method, MethodSelector cs) {
		
		this.ms = cs;

		_interface = ((method.access & ACC_ABSTRACT) != 0);
		this.className = className;
		this.method = method;

		if (!_interface) {
			this.memory = new MemoryManager(method.maxLocals, method.maxStack);
			this.state = new StateCodeGenerator(code, memory);
		}

		if ((method.access & ACC_STATIC) != 0) {
			_static = true;
		}

	}

	public MethodNode generate() throws AnalyzerException {

		if (!_interface) {
			Analyzer analyzer = new Analyzer(new BasicInterpreter());
			frames = analyzer.analyze(className, method);
		}
		String desc = "(" + Utils.continuationClassDesc + ")"
				+ Utils.continuationClassDesc;

		String name = method.name + method.desc;

		if ((method.access & ACC_PRIVATE) != 0) {
			name += "_private_" + className;
		}

		if (_interface) {
			return new MethodNode(ACC_PUBLIC | ACC_ABSTRACT, name, desc,
					null /* ? */, null);
		}
		MethodNode cont;
		if (_static) {
			cont = new MethodNode(ACC_PUBLIC | ACC_STATIC, name, desc,
					null /* ? */, null);
		} else {
			cont = new MethodNode(ACC_PUBLIC, name, desc, null /* ? */, null);
		}

		generateCode();
		cont.instructions.add(code);

		@SuppressWarnings("unchecked")
		List<TryCatchBlockNode> tcbs = method.tryCatchBlocks;
		for (TryCatchBlockNode tcb : tcbs) {

			LabelNode start = labelMap.get(tcb.start);
			LabelNode end = labelMap.get(tcb.end);
			LabelNode handler = labelMap.get(tcb.handler);
			String type = tcb.type;

			@SuppressWarnings({ "unchecked", "unused" })
			boolean add = cont.tryCatchBlocks.add(new TryCatchBlockNode(start, end, handler,
					type));

		}
		@SuppressWarnings({ "unchecked", "unused" })
		boolean add = cont.tryCatchBlocks.add(new TryCatchBlockNode(start, end, handler,
				"java/lang/Throwable"));
		return cont;
	}

	private void generateCode() {

		LabelNode l0 = new LabelNode(new Label());
		labels.add(l0);

		code.add(l0);

		state.restoreLocals(frames[0]);

		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		code.add(new InsnNode(DUP));
		StateCodeGenerator.getLocals(code);
		code.add(new LdcInsnNode(method.maxLocals + 5));
		code.add(new MethodInsnNode(INVOKESTATIC, "java/util/Arrays", "copyOf",
				"([Ljava/lang/Object;I)[Ljava/lang/Object;", false));
		StateCodeGenerator.putLocals(code);

		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		code.add(new LdcInsnNode(method.maxStack + 5));
		code.add(new TypeInsnNode(ANEWARRAY, "Ljava/lang/Object;"));
		StateCodeGenerator.putStack(code);

		InsnList originalCode = method.instructions;

		for (AbstractInsnNode insn : originalCode.toArray()) {
			if (insn.getType() == AbstractInsnNode.LABEL) {
				labelMap.put((LabelNode) insn, new LabelNode(new Label()));
			}
		}

		int i = 0;
		for (AbstractInsnNode insn : originalCode.toArray()) {
			processInsn(insn, i);
			i++;
		}

		code.add(end);

		code.add(handler);

		handleException();

		code.insert(generateSwitchIpCode());

	}

	private void handleException() {

		memory.storeTemp(code, "Ljava/lang/Throwable;", 1);
		state.cloneParent(code);
		code.add(new InsnNode(DUP));

		code.add(new InsnNode(DUP));
		LabelNode parentNull = new LabelNode(new Label());
		code.add(new JumpInsnNode(IFNULL, parentNull));

		memory.loadTemp(code, "Ljava/lang/Throwable;", 1);
		StateCodeGenerator.putField(code, "resultException",
				"Ljava/lang/Throwable;");
		code.add(new InsnNode(ARETURN));

		code.add(parentNull);
		memory.loadTemp(code, "Ljava/lang/Throwable;", 1);
		code.add(new InsnNode(ATHROW));

	}

	private void processInsn(AbstractInsnNode insn, int i) {

		int opcode = insn.getOpcode();

		switch (insn.getType()) {

		case AbstractInsnNode.METHOD_INSN:
			processMethodInsn((MethodInsnNode) insn, frames[i], frames[i + 1]);
			return;

		case AbstractInsnNode.FIELD_INSN:
			processFieldInsn((FieldInsnNode) insn, frames[i]);
			return;

		case AbstractInsnNode.INSN:

			boolean aload = opcode >= IALOAD && opcode <= SALOAD;
			boolean astore = opcode >= IASTORE && opcode <= SASTORE;

			if (aload || astore) {
				processArrayInsn((InsnNode) insn, astore, frames[i]);
				return;
			}

			if (opcode >= IRETURN && opcode <= RETURN) {

				state.cloneParent(code);

				memory.storeTemp(code, "Ljava/lang/Object;", 3);

				if (opcode != RETURN) {
					Type returnType = Type.getMethodType(method.desc)
							.getReturnType();
					String returnDesc = returnType.getDescriptor();

					memory.storeTemp(code, returnDesc, 1);

					memory.loadTemp(code, "Ljava/lang/Object;", 3);
					StateCodeGenerator.getStack(code);

					memory.loadTemp(code, "Ljava/lang/Object;", 3);
					StateCodeGenerator.getField(code, "stackSize", "I");

					memory.loadTemp(code, returnDesc, 1);

					if (returnType.getSort() != Type.OBJECT) {
						memory.box(code, returnDesc);
					}

					memory.storeTemp(code, "Ljava/lang/Object;", 1);

					memory.loadTemp(code, "Ljava/lang/Object;", 0);
					memory.loadTemp(code, "Ljava/lang/Object;", 1);
					StateCodeGenerator.putField(code, "result",
							"Ljava/lang/Object;");
					
					memory.loadTemp(code, "Ljava/lang/Object;", 3);
					code.add(new LdcInsnNode(true));
					StateCodeGenerator.putField(code, "end", "Z");

					memory.loadTemp(code, "Ljava/lang/Object;", 1);
					code.add(new InsnNode(AASTORE));
				}

				memory.loadTemp(code, "Ljava/lang/Object;", 3);
				code.add(new InsnNode(ARETURN));

				return;

			}

			// intentional fall-through

		default:

			code.add(insn.clone(labelMap));
		}

	}

	private void processArrayInsn(InsnNode insn, boolean store, Frame frame) {

		BasicValue topValue = null;
		String topDesc = null;

		if (store) {
			topValue = ((BasicValue) frame.getStack(frame.getStackSize() - 1));
			topDesc = topValue.getType().getDescriptor();

			memory.storeTemp(code, topDesc, 1);
		}

		memory.storeTemp(code, "I", 3);

		String desc = "";
		switch (insn.getOpcode()) {
		case AALOAD:
		case AASTORE:
			desc = "[Ljava/lang/Object;";
			break;
		case BALOAD:
		case BASTORE:
			desc = "[B";
			break;
		case CALOAD:
		case CASTORE:
			desc = "[C";
			break;
		case DALOAD:
		case DASTORE:
			desc = "[D";
			break;
		case FALOAD:
		case FASTORE:
			desc = "[F";
			break;
		case IALOAD:
		case IASTORE:
			desc = "[I";
			break;
		case LALOAD:
		case LASTORE:
			desc = "[L";
			break;
		case SASTORE:
		case SALOAD:
			desc = "[S";
			break;
		}

		code.add(new TypeInsnNode(CHECKCAST, desc));

		memory.loadTemp(code, "I", 3);

		if (store) {
			memory.loadTemp(code, topDesc, 1);
		}

		code.add(insn.clone(labelMap));

	}

	private void processFieldInsn(FieldInsnNode insn, Frame frame) {

		int opcode = insn.getOpcode();

		boolean _static = false;
		boolean put = false;

		if (opcode == PUTFIELD || opcode == PUTSTATIC) {
			put = true;
		}
		if (opcode == GETSTATIC || opcode == PUTSTATIC) {
			_static = true;
		}

		BasicValue value = null;
		String desc = null;

		if (put) {
			value = ((BasicValue) frame.getStack(frame.getStackSize() - 1));
			desc = value.getType().getDescriptor();
		}

		if (put && !_static) {
			memory.storeTemp(code, desc, 1);
		}

		if (!_static) {
			code.add(new TypeInsnNode(CHECKCAST, insn.owner));
		}

		if (put && !_static) {
			memory.loadTemp(code, desc, 1);

		}

		if (put) {
			if (value.isReference()) {
				code.add(new TypeInsnNode(CHECKCAST, insn.desc));
			}
		}

		code.add(insn.clone(labelMap));

	}

	private void processMethodInsn(MethodInsnNode insn, Frame before,
			Frame after) {

		code.add(new LdcInsnNode(insn.owner.replace('/', '.')));
		code.add(new MethodInsnNode(INVOKESTATIC, "java/lang/Class", "forName",
				"(Ljava/lang/String;)Ljava/lang/Class;", false));
		code.add(new InsnNode(POP));

		if (!ms.transform(insn.owner, insn.name, insn.desc)
				&& !(insn.owner.equals(Utils.apiPackageName + "/Continuations"))) {
			state.prepareCall(before, insn);
			code.add(insn.clone(labelMap));
			return;
		}

		boolean shift = insn.name.equals("shift")
				&& insn.owner.equals(Utils.apiPackageName + "/Continuations");

		state.saveLocals(before);
		state.saveStack(before, insn);

		if (shift) {
			memory.loadTemp(code, "Ljava/lang/Object;", 0);
			StateCodeGenerator.getStack(code);
			code.add(new LdcInsnNode(before.getStackSize()));
			memory.loadTemp(code, "Ljava/lang/Object;", 0);
			code.add(new InsnNode(AASTORE));
		}

		SetIp(labels.size());

		generateCall(insn, before, after);

		LabelNode l0 = new LabelNode(new Label());
		labels.add(l0);
		code.add(l0);

		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		StateCodeGenerator.getField(code, "resultException",
				"Ljava/lang/Throwable;");
		code.add(new InsnNode(DUP));

		LabelNode noException = new LabelNode(new Label());
		code.add(new JumpInsnNode(IFNULL, noException));

		code.add(new InsnNode(ATHROW));

		code.add(noException);

		code.add(new InsnNode(POP));
		state.restoreLocals(after);

		state.restoreStack(after, null);

	}

	private void SetIp(int ip) {
		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		code.add(new LdcInsnNode(ip));
		code.add(new FieldInsnNode(PUTFIELD, Utils.continuationClassName, "ip",
				"I"));
	}

	private void generateCall(MethodInsnNode insn, Frame before, Frame after) {

		boolean reset = insn.name.equals("reset")
				&& insn.owner.equals(Utils.apiPackageName + "/Continuations");

		boolean shift = insn.name.equals("shift")
				&& insn.owner.equals(Utils.apiPackageName + "/Continuations");

		if (reset) {
			insn = new MethodInsnNode(INVOKEINTERFACE, Utils.apiPackageName + "/Callable",
					"call", "()Ljava/lang/Object;", true);
		}

		if (shift) {
			insn = new MethodInsnNode(INVOKEINTERFACE, Utils.apiPackageName +  "/Function",
					"call", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
		}

		String contClassName = Utils.packageName + "/" + insn.owner + "/"
				+ insn.name + insn.desc.replace(';', ')').replace(']', ')') + "_Continuation";
		code.add(new TypeInsnNode(NEW, contClassName));

		code.add(new InsnNode(DUP));

		code.add(new MethodInsnNode(INVOKESPECIAL, contClassName, "<init>",
				"()V", false));

		code.add(new InsnNode(DUP));

		Type t = Type.getMethodType(insn.desc);
		int nArgs = t.getArgumentTypes().length;

		int start = before.getStackSize() - nArgs;
		int end = before.getStackSize();

		if (insn.getOpcode() != INVOKESTATIC) {
			start--;
		}

		if (shift) {
			start++;
			end++;
		}

		memory.loadTemp(code, "Ljava/lang/Object", 0);
		StateCodeGenerator.getStack(code);
		code.add(new MethodInsnNode(INVOKESTATIC, "java/util/Arrays", "asList",
				"([Ljava/lang/Object;)Ljava/util/List;", false));

		code.add(new LdcInsnNode(start));
		code.add(new LdcInsnNode(end));

		code.add(new MethodInsnNode(INVOKEINTERFACE, "java/util/List",
				"subList", "(II)Ljava/util/List;", true));
		code.add(new MethodInsnNode(INVOKEINTERFACE, "java/util/List",
				"toArray", "()[Ljava/lang/Object;", true));

		StateCodeGenerator.putLocals(code);

		code.add(new InsnNode(DUP));
		code.add(new LdcInsnNode(insn.getOpcode()));
		StateCodeGenerator.putField(code, "opcode", "I");

		code.add(new InsnNode(DUP));
		code.add(new LdcInsnNode(insn.itf));
		StateCodeGenerator.putField(code, "itf", "Z");

		if (reset || shift) {
			code.add(new InsnNode(DUP));
			code.add(new LdcInsnNode(true));
			StateCodeGenerator.putField(code, "reset", "Z");
		}

		if (insn.getOpcode() != INVOKESTATIC) {
			code.add(new InsnNode(DUP));
			code.add(new InsnNode(DUP));
			StateCodeGenerator.getLocals(code);
			code.add(new LdcInsnNode(0));
			code.add(new InsnNode(AALOAD));

			StateCodeGenerator.putField(code, "_this", "Ljava/lang/Object;");
		}

		code.add(new InsnNode(DUP));
		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		if (shift) {
			LabelNode findReset = new LabelNode(new Label());
			code.add(findReset);

			LabelNode resetFound = new LabelNode(new Label());
			LabelNode resetNotFound = new LabelNode(new Label());

			code.add(new InsnNode(DUP));
			code.add(new JumpInsnNode(IFNULL, resetNotFound));

			code.add(new InsnNode(DUP));

			StateCodeGenerator.getField(code, "reset", "Z");
			code.add(new JumpInsnNode(IFNE, resetFound));

			StateCodeGenerator.getField(code, "parent",
					"Lmt/internal/Continuation;");
			code.add(new JumpInsnNode(GOTO, findReset));

			code.add(resetNotFound);

			code.add(new TypeInsnNode(NEW, "java/lang/RuntimeException"));
			code.add(new InsnNode(DUP));
			code.add(new LdcInsnNode("Shift outside of reset."));
			code.add(new MethodInsnNode(INVOKESPECIAL,
					"java/lang/RuntimeException", "<init>",
					"(Ljava/lang/String;)V", false));
			code.add(new InsnNode(ATHROW));

			code.add(resetFound);

		}
		StateCodeGenerator
				.putField(code, "parent", Utils.continuationClassDesc);

		memory.loadTemp(code, "Ljava/lang/Object;", 0);
		code.add(new LdcInsnNode(after.getStackSize() - 1));
		StateCodeGenerator.putField(code, "stackSize", "I");

		code.add(new InsnNode(ARETURN));

	}

	private InsnList generateSwitchIpCode() {

		InsnList switchCode = new InsnList();

		// save state to temp
		memory.loadLocal(switchCode, "[Ljava/lang/Object;", _static ? 0 : 1);
		memory.storeTemp(switchCode, "[Ljava/lang/Object;", 0);

		switchCode.add(start);

		memory.loadTemp(switchCode, "[Ljava/lang/Object;", 0);

		switchCode.add(new FieldInsnNode(GETFIELD, Utils.continuationClassName,
				"ip", "I"));

		LabelNode[] labelsArray = new LabelNode[labels.size()];
		labels.toArray(labelsArray);

		switchCode.add(new TableSwitchInsnNode(0, labels.size() - 1, labels
				.get(0), labelsArray));

		return switchCode;

	}

}
