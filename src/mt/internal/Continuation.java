package mt.internal;

import java.util.Arrays;

public class Continuation<A,B> implements mt.api.Continuation<A,B>, Cloneable {

	public Object[] stack;
	public int stackSize;
	public Object[] locals;
	public int ip;

	public int opcode;
	public boolean itf;

	public Object _this;

	public Continuation<?,?> parent;

	public boolean reset;
	public boolean end;
	
	public B result;
	public Throwable resultException;

	public Continuation<?,?> clone() throws CloneNotSupportedException {
		try {
			Continuation<?,?> copy = this.getClass().newInstance();
			if (stack != null) {
				copy.stack = Arrays.copyOf(stack, stack.length);
			}
			copy.stackSize = stackSize;
			if (locals != null) {
				copy.locals = Arrays.copyOf(locals, locals.length);
			}
			copy.ip = ip;
			copy.opcode = opcode;
			copy.itf = itf;
			copy._this = _this;
			copy.parent = parent;
			copy.reset = reset;

			return copy;
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public Continuation<?, ?> Continue() {
		return null;
	}

	@Override
	public B call(A arg) throws Exception {
		try {
			Continuation<?, ?> continuation = (Continuation<?, ?>) clone();
			continuation.stack[continuation.stackSize] = arg;
			return Trampoline.runToReset(continuation);
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}



}
