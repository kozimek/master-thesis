package mt.internal;

import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TableSwitchInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

public class ContinuationBuilder {

	private String className;
	private MethodNode method;
	MemoryManager memory = new MemoryManager(5, 5);

	boolean _static = false;

	ClassNode cont;
	MethodNode call;

	public ContinuationBuilder(String className, MethodNode method) {

		this.className = className;
		this.method = method;

		if ((method.access & Opcodes.ACC_STATIC) != 0) {
			_static = true;
		}
	}

	ClassNode build() {
		createClass();
		build_Continue();

		return cont;
	}

	private void build_Continue() {
		call = new MethodNode(Opcodes.ACC_PUBLIC, "Continue", "()"
				+ Utils.continuationClassDesc, null, null);

		@SuppressWarnings({ "unchecked", "unused" })
		boolean add = cont.methods.add(call);

		InsnList code = call.instructions;

		// call
		String name = method.name + method.desc;

		if ((method.access & Opcodes.ACC_PRIVATE) != 0) {
			name += "_private_" + className;
		}

		String desc = "(" + Utils.continuationClassDesc + ")"
				+ Utils.continuationClassDesc;

		LabelNode afterCall = new LabelNode(new Label());

		LabelNode labels[] = new LabelNode[4];
		for (int i = 0; i < 4; i++) {
			labels[i] = new LabelNode(new Label());
		}

		memory.loadLocal(code, "Ljava/lang/Object;", 0);
		StateCodeGenerator.getField(code, "opcode", "I");

		code.add(new TableSwitchInsnNode(Opcodes.INVOKEVIRTUAL,
				Opcodes.INVOKEINTERFACE, labels[0], labels));

		for (int i = 0, j = Opcodes.INVOKEVIRTUAL; i < 4; i++, j++) {

			code.add(labels[i]);
			// arguments:
			// this
			if (j != Opcodes.INVOKESTATIC) {
				memory.loadLocal(code, "Ljava/lang/Object;", 0);
				StateCodeGenerator
						.getField(code, "_this", "Ljava/lang/Object;");
				code.add(new TypeInsnNode(Opcodes.CHECKCAST, className));
			}

			// state
			memory.loadLocal(code, "Ljava/lang/Object;", 0);

			switch (j) {
			case Opcodes.INVOKESPECIAL:
				code.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, className,
						name, desc, false));
				break;
			case Opcodes.INVOKEINTERFACE:
				code.add(new MethodInsnNode(j, className, name, desc, true));
				break;
			case Opcodes.INVOKESTATIC:
				code.add(new MethodInsnNode(j, className, name, desc, false));
				break;
			case Opcodes.INVOKEVIRTUAL:
				code.add(new MethodInsnNode(j, className, name, desc, false));
			}

			code.add(new JumpInsnNode(Opcodes.GOTO, afterCall));
		}

		code.add(afterCall);
		code.add(new InsnNode(Opcodes.ARETURN));

	}

	private void createClass() {
		cont = new ClassNode();
		cont.access = Opcodes.ACC_PUBLIC;
		cont.name = Utils.packageName + "/" + className + "/" + method.name
				+ method.desc.replace(';', ')').replace('[', '(')
				+ "_Continuation";
		cont.superName = Utils.continuationClassName;

		cont.version = Opcodes.V1_7;

		MethodNode init = new MethodNode(Opcodes.ACC_PUBLIC, "<init>", "()V",
				null, null);
		init.instructions.add(new VarInsnNode(Opcodes.ALOAD, 0));
		init.instructions.add(new MethodInsnNode(Opcodes.INVOKESPECIAL,
				Utils.continuationClassName, "<init>", "()V", false));
		init.instructions.add(new InsnNode(Opcodes.RETURN));

		@SuppressWarnings({ "unchecked", "unused" })
		boolean _ = cont.methods.add(init);

	}

}
