To build the library, type **ant build**
To run tests, type **ant test**
----
To run a program using the library, make sure that you have Agent.jar in your classpath, as well as set as javaagent. ASM library must be available in bootclasspath. These are the options needed: 

* -cp path/to/Agent.jar
* -javaagent:path/to/Agent.jar
* -Xbootclasspath/a:path/to/asm-all-version.jar